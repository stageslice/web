import Vue from 'vue'
import VueButler from 'vue-butler'

Vue.use(VueButler, {
  components: '*'
})
