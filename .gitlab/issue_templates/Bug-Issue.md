## Description
( A general description )

## How to reproduce
( What have i done to produce the bug )

## To Do
( A list of all the requirements with description )

## Possible Solutions


## Checklist
( Your Checkpoints )
* [ ]  Create Test who reproduce Bug 
* [ ]  Unit Tests passes
* [ ]  Linting passes

/label ~Bug 
