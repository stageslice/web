module.exports = {
  apps: [{
    name: 'SliceWebFrontend',
    script: './node_modules/nuxt-start/bin/nuxt-start.js',
    // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file
    instances: 'max',
    port: 3000,
    exec_mode: 'cluster',
    env: {
      NODE_ENV: 'development'
    },
    env_production: {
      NODE_ENV: 'production'
    }
  }]
}
